var Nightmare = require('./BrandNewNightmare');
var fs = require('fs');
var vo = require('vo');

function facebook() {
    this.searchName = function (searchInput) {

        // create nightmare object with type interval 20 and set chrome as its useragent
        var nightmare = Nightmare({
            typeInterval: 20,
            pollInterval: 0,
            // show: true
        }).useragent('chrome');
        // try to open cookie file inorder to check if it is empty or not
        const stats = fs.statSync("app/public/cookies/facebook/cookie.json")
        const fileSizeInBytes = stats.size;
        if (fileSizeInBytes == 0) {
            // becuase cookie file is empty there is no need to continue
            return false;
        }

        // open cookie file and read it to set it on the agent
        var cookies_raw = fs.readFileSync('app/public/cookies/facebook/cookie.json', 'utf8');
        var cookies = JSON.parse(cookies_raw);

        // return nightmare object to test it's result 
        return nightmare.cookies.clearAll()
            .gotoReady('https://www.facebook.com/')
            .cookies.set(cookies)
            .gotoReady(`https://www.facebook.com/search/people/?q=${searchInput}`)
            .wait('div._3u1')
            .evaluate(() => {
                var result = [];
                document.querySelectorAll('div._3u1').forEach(function (el) {

                    var subResult = {}
                    if (el.querySelector('img._1glk.img') != null)
                        subResult['image'] = el.querySelector('img._1glk.img').src;
                    if (el.querySelector('a._32mo span') != null)
                        subResult['name'] = el.querySelector('a._32mo span').innerHTML;
                    if (el.querySelector('div._pac a') != null)
                        subResult['other'] = el.querySelector('div._pac a').innerHTML;
                    if (el.querySelector('a') != null)
                        subResult['link'] = el.querySelector('a').href;
                    if (el.querySelectorAll('div._glo div._ajw div._52eh') != null)
                        var layer3Result = []
                    el.querySelectorAll('div._glo div._ajw div._52eh').forEach(function (sub_el) {

                        layer3Result.push(sub_el.innerHTML.replace(/(<([^>]+)>)/ig, ""));
                    });
                    subResult['description'] = layer3Result;

                    result.push(subResult);
                });
                return result
            })
            .end()
            .then(res => res)
    }

    this.searchMobileNumber = function (searchInput) {
        // create nightmare object with type interval 20 and set chrome as its useragent
        var nightmare = Nightmare({
            typeInterval: 20,
            pollInterval: 0,
            // show: true
        }).useragent('chrome');

        // return nightmare object to test it's result 
        return nightmare.cookies.clearAll()
            .gotoReady('https://www.facebook.com/')
            .click('a[href="https://www.facebook.com/recover/initiate?lwv=110"]').wait('input#identify_email.inputtext')
            .type("input#identify_email.inputtext", searchInput)
            .click('input#u_0_2')
            .wait(4000)
            .evaluate(function () {
                var result = [];
                let subResult = {};
                if (document.querySelector('div.fsl.fwb.fcb') != null)
                    subResult['name'] = document.querySelector('div.fsl.fwb.fcb').innerHTML;
                if (document.querySelector('img._s0._rw.img') != null)
                    subResult['image'] = document.querySelector('img._s0._rw.img').src;
                result.push(subResult);
                return result;
            })
            .end()
            .then(res => res);
    }

}

module.exports = facebook;