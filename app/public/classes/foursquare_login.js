var Nightmare = require('./BrandNewNightmare');
var fs = require('fs');


function foursquare_login() {
    this.checkLogin = function() {
        
        // create nightmare object with type interval 20 and set chrome as its useragent
        var nightmare = Nightmare({
            typeInterval: 20,
            pollInterval: 50,
            // show : true
            }).useragent('chrome');
        // try to open cookie file inorder to check if it is empty or not
        const stats = fs.statSync("app/public/cookies/foursquare/cookie.json")
        const fileSizeInBytes = stats.size;
        if(fileSizeInBytes == 0 ){
            // becuase cookie file is empty there is no need to continue
            return false;
        } 
        
        // open cookie file and read it to set it on the agent
        var cookies_raw = fs.readFileSync('app/public/cookies/foursquare/cookie.json','utf8');        
        var cookies = JSON.parse(cookies_raw);

        // return nightmare object to test it's result 
        return nightmare
        .cookies.clearAll()
        .gotoReady('https://www.foursquare.com/')
        .cookies.set(cookies)
        .refresh()
        .wait(1000)
        // .wait('a[data-control-name="identity_profile_photo"]')
        .evaluate(function(){
            return document.querySelector('a.userPathLink').innerHTML;
        })
        .end()
        .then(function(res){
            // return 'App is still logged in with username : '+ res;
            return true;
        })
        .catch(function(err){
            // return 'App is not logged in and must again try to sign in.';
            return false;
        });

  }

  this.doLogin = function(){

        // create nightmare object with type interval 20 and set chrome as its useragent
        var nightmare = Nightmare({
        typeInterval: 20,
        // show: true
        }).useragent('chrome');
        // open main page of foursquare and insert our login inputs and return it 
        return nightmare.cookies.clearAll().goto('https://foursquare.com/login')
        .type('input#username.formStyle','fashadam.moosavi@gmail.com')
        .type('input#password.formStyle','magenagoftam')
        .click('button#loginFormButton')
        .wait(6000)
        .cookies.get(function(res){
            
        })
        .end()
        .then(function(res){
            // to save cookie file we need to write current cookie as our main cookie
            fs.writeFile("app/public/cookies/foursquare/cookie.json", JSON.stringify(res), 'utf8', function(error) {
                return true;
            });
            // return output message
            return "App logged in successfully";
        })
        .catch(()=>'App could not login');
  }

}

module.exports = foursquare_login;