var Nightmare = require('./BrandNewNightmare');
var fs = require('fs');
var vo = require('vo');

function myspace() {
    this.searchName = function (searchInput) {

        // create nightmare object with type interval 20 and set chrome as its useragent
        var nightmare = Nightmare({
            typeInterval: 20,
            pollInterval: 0,
            // show: true
        }).useragent('chrome');
       
        // return nightmare object to test it's result 
        return nightmare.cookies.clearAll()
            .gotoReady(`https://myspace.com/search/people?q=${searchInput}`)
            .wait('ul.people.grid.infinite')
            .evaluate(() => {
                var result = [];
                document.querySelectorAll('ul.people.grid.infinite li').forEach(function(el){

                    let subResult = {}
					if(el.querySelector('img') != null){
                    subResult['image'] =  el.querySelector('img').src;
                    subResult['name'] =  el.querySelector('div > h6 > a').innerHTML;
                    subResult['link'] =  el.querySelector('div a').href;
                    }
                    
                    result.push(subResult);
                });
                return result
            })
            .end()
            .then(res=>res)
    }

}

module.exports = myspace;