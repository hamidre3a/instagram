var Nightmare = require('./BrandNewNightmare');
var fs = require('fs');
var vo = require('vo');
var run = function* (searchInput) {

    // create nightmare object with type interval 20 and set chrome as its useragent
    yield nightmare = Nightmare({
        typeInterval: 20,
        pollInterval: 0,
        // show: true
    }).useragent('chrome');
    // try to open cookie file inorder to check if it is empty or not
    const stats = fs.statSync("app/public/cookies/atbox/cookie.json")
    const fileSizeInBytes = stats.size;
    if (fileSizeInBytes == 0) {
        // becuase cookie file is empty there is no need to continue
        return false;
    }

    // open cookie file and read it to set it on the agent
    var cookies_raw = fs.readFileSync('app/public/cookies/atbox/cookie.json', 'utf8');
    var cookies = JSON.parse(cookies_raw);

    // return nightmare object to test it's result 
    yield nightmare.cookies.clearAll()
        .gotoReady('https://www.atbox.io/')
        .cookies.set(cookies)
        .goto(`https://www.atbox.io/search#${searchInput}`)
        .wait('div#search-result li')

    var goToHeight = 400;
    var currentHeight = 0;
    var currentHeight = yield nightmare.evaluate(function () {
        return document.body.scrollHeight;
    });
    while (goToHeight < currentHeight) {
        currentHeight = yield nightmare.evaluate(function () {
            return document.body.scrollHeight;
        });
        yield nightmare.scrollTo(goToHeight, 0)
            .wait(200);
        goToHeight = goToHeight + 40;
    }

    return nightmare.evaluate(() => {
        var result = [];
        document.querySelectorAll('div#search-result li').forEach(function (el) {
            let subResult = {}
            if (el.querySelector('a img') != null)
                subResult['image'] = el.querySelector('a img').src;
            if (el.querySelector('h2') != null)
                subResult['name'] = el.querySelector('h2').innerText;
            if (el.querySelector('a') != null)
                subResult['link'] = el.querySelector('a').href;
            if (el.querySelector('h3') != null)
                subResult['description'] = el.querySelector('h3').innerText;
            result.push(subResult);
        });

        return result
    })
    .end()
    .then(res => res)

};
function atbox() {
    this.searchName = function (searchInput) {
        return vo(run(searchInput)).then((res) => {
            return res;
        })
            .catch(e => console.error(e))
    }


}

module.exports = atbox;