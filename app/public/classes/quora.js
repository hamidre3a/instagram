var Nightmare = require('./BrandNewNightmare');
var fs = require('fs');
var vo = require('vo');

function quora() {
    this.searchName = function (searchInput) {

        // create nightmare object with type interval 20 and set chrome as its useragent
        var nightmare = Nightmare({
            typeInterval: 20,
            pollInterval: 0,
            show: true
        }).useragent('chrome');

        // return nightmare object to test it's result 
        return nightmare.cookies.clearAll()
            .gotoReady(`https://www.quora.com/search?q=profile%3A+%D8%A7%D8%AD%D9%85%D8%AF`)
            .wait('input[placeholder="Find People"]')
            .type('input[placeholder="Find People"]', searchInput)
            .evaluate(() => {
                var result = [];
                document.querySelectorAll('li.selector_result').forEach(function (el) {
                    let subResult = {}
                    if (el.querySelector('img') != null)
                        subResult['image'] = el.querySelector('img').src;
                    if (el.querySelector('img') != null)
                        subResult['name'] = el.querySelector('img').alt;
                    if (el.querySelector('span.photo_tooltip a') != null)
                        subResult['link'] = el.querySelector('span.photo_tooltip a').href;
                    result.push(subResult);
                });

                return result;

            })
            .end()
            .then(res => res)
    }

}

module.exports = quora;