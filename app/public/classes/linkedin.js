var Nightmare = require('./BrandNewNightmare');
var fs = require('fs');
var vo = require('vo');

function linkedin() {
    this.searchName = function (searchInput) {

        // create nightmare object with type interval 20 and set chrome as its useragent
        var nightmare = Nightmare({
            typeInterval: 20,
            // pollInterval: 0,
            // show: true
        }).useragent('chrome');
        // try to open cookie file inorder to check if it is empty or not
        const stats = fs.statSync("app/public/cookies/linkedin/cookie.json")
        const fileSizeInBytes = stats.size;
        if (fileSizeInBytes == 0) {
            // becuase cookie file is empty there is no need to continue
            return false;
        }

        // open cookie file and read it to set it on the agent
        var cookies_raw = fs.readFileSync('app/public/cookies/linkedin/cookie.json', 'utf8');
        var cookies = JSON.parse(cookies_raw);

        // return nightmare object to test it's result 
        return nightmare.cookies.clearAll()
            .gotoReady('https://www.linkedin.com/')
            .cookies.set(cookies)
            .goto(`https://www.linkedin.com/search/results/people/?keywords=${searchInput}&origin=GLOBAL_SEARCH_HEADER`)
            .wait('div.search-result__wrapper')
            .scrollTo(4000,0)
            .wait()
            .evaluate(() => {
                var result = [];
                document.querySelectorAll('div.search-result__wrapper').forEach(function (el) {
                    let subResult = {}
                    if(el.querySelector('img.lazy-image.loaded') != null)
                        subResult['image'] = el.querySelector('img.lazy-image.loaded').src;
                    if(el.querySelector('span.name.actor-name') != null)
                        subResult['name'] = el.querySelector('span.name.actor-name').innerHTML;
                    if(el.querySelector('a') != null)
                        subResult['link'] = el.querySelector('a').href;
                    if(el.querySelector('p.subline-level-1') != null)
                        subResult['description'] = el.querySelector('p.subline-level-1').innerHTML;
                    if(el.querySelector('p.subline-level-2') != null)
                        subResult['country'] = el.querySelector('p.subline-level-2').innerHTML;

                    result.push(subResult);
                });
                return result;
            })
            .end()
            .then(res => res)
    }

}

module.exports = linkedin;