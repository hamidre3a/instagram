var Nightmare = require('./BrandNewNightmare');
var fs = require('fs');
var vo = require('vo');
var count;

var followings = function* (nightmare) {

    // return nightmare object to test it's result 
    yield nightmare
        .goto(`https://www.instagram.com/reservina.ir/`)

    followingNumber = yield nightmare.evaluate(function () {
        if (document.querySelector('a[href="/reservina.ir/following/"]._t98z6').innerText.replace(' followings') != null)
            return parseInt(document.querySelector('a[href="/reservina.ir/following/"]._t98z6').innerText.replace(' followings'));
        else
            return 0;
    });


    isItFollowable = yield nightmare.evaluate(function () {
        if (document.querySelector('a[href="/reservina.ir/following/"]._t98z6') != null)
            return true;
        else
            return false;
    });
    if (isItFollowable) {
        yield nightmare
            .click('a[href="/reservina.ir/following/"]._t98z6')
            .wait(2000)
    }
    // var followers = 0;
    // var TryCount = 0;
    // while (followingNumber >= followers+20 && TryCount < 2000  ) {
    //     followers = yield nightmare.evaluate(function () {
    //         return document.querySelectorAll('div._gs38e li').length;
    //     });

    //     followers
    //     TryCount++;
    //     yield nightmare.evaluate(function () {
    //         if(document.querySelector('div._gs38e') != null)
    //             document.querySelector('div._gs38e').scrollTop = 30000 * 30000;
    //         return false;
    //         // return  document.querySelectorAll('div._gs38e');
    //     });

    // }
    var goToHeight = 400;
    var lastNum = 0;
    var loop = 0;
    var followers = 0;
    while (loop < 200) {
        followers = yield nightmare.evaluate(function () {
            return document.querySelectorAll('div._gs38e li').length;
        });

        if (followers == lastNum)
            loop++;
        else
            loop = 0;

        lastNum = followers;
        yield nightmare.evaluate(function () {
            if (document.querySelector('div._gs38e') != null)
                document.querySelector('div._gs38e').scrollTop = 30000 * 30000;
            return false;
            // return  document.querySelectorAll('div._gs38e');
        });
    }

    yield nightmare.evaluate(function () {
        let buttons = document.querySelectorAll('div._gs38e li span._ov9ai button');
        if (buttons.length > 10) {
            for (var i = buttons.length - 1; i > buttons.length - 21; i--)
                buttons[i].click();
        }


        return false;
        // return  document.querySelectorAll('div._gs38e');
    });
    yield nightmare.wait();
    return {};

};
function onlyUnique(a) {
    var flags = [], output = [], l = a.length, i;
    for (i = 0; i < l; i++) {
        if (flags[a[i].link]) continue;
        flags[a[i].link] = true;
        output.push({ "link": a[i].link });
    }
    return output;
}
var posts = function* (searchInput, nightmare) {
    yield vo(followings(nightmare)).then((res) => {
        return res;
    })
    // return nightmare object to test it's result 
    yield nightmare
        // .goto(`https://www.instagram.com/explore`)
        .goto(`https://www.instagram.com/explore/tags/${searchInput}/`)

    var posts = [];
    var all_posts = [];
    while (onlyUnique(all_posts).length < 200) {

        posts.push(yield nightmare
            .evaluate(function () {
                var result = [];
                document.querySelectorAll('div._mck9w._gvoze')
                    .forEach(function (el) {
                        let subResult = {}
                        if (el.querySelector('a').href != null)
                            subResult['link'] = el.querySelector('a').href;
                        result.push(subResult);
                    });
                return result;
            }));

        for (i = 0; i < posts.length; i++) {
            for (j = 0; j < posts[i].length; j++) {
                all_posts.push(posts[i][j]);
            }
            // yield vo(singleArticlePost(posts[i].link, nightmare))
        }
        // console.log(onlyUnique(all_posts).length);

        yield nightmare.scrollTo(9000 * 90000, 0);

    }

    // return {posts};
    // var posts = yield nightmare
    //     .wait(2000)
    //     .evaluate(function () {
    //         var result = [];
    //         document.querySelectorAll('div._mck9w._gvoze')
    //             .forEach(function (el) {
    //                 let subResult = {}
    //                 if (el.querySelector('a').href != null)
    //                     subResult['link'] = el.querySelector('a').href;
    //                 result.push(subResult);
    //             });
    //         return result;
    //     })
    // yield nightmare.then(function (res) {
    //     return res;
    // })
    //     .catch(function (err) {
    //         return {};
    //     });
    yield vo(followings(nightmare)).then((res) => {
        return res;
    })
    all_posts = onlyUnique(all_posts);
    var finalPosts = [];
    for (i = 0; i < all_posts.length; i++) {
        let temp_posts = {};
        yield vo(singleArticlePost(all_posts[i].link, nightmare))
    }
    yield vo(followings(nightmare)).then((res) => {
        return res;
    })

    yield nightmare.end();
    return { all_posts };

};

var singleArticlePost = function* (link, nightmare) {

    // return nightmare object to test it's result 
    yield nightmare
        .goto(link)
        .wait()

    isItLikable = yield nightmare.evaluate(function () {
        if (document.querySelector('span.coreSpriteHeartOpen') != null)
            return true;
        else
            return false;
    });
    isItFollowable = yield nightmare.evaluate(function () {
        if (document.querySelector('button._qv64e._iokts._4tgw8') != null)
            return true;
        else
            return false;
    });
    if (isItLikable) {
        yield nightmare
            .click('span.coreSpriteHeartOpen')
    }

    if (isItFollowable && count < 50) {
        count++;
        yield nightmare
            .click('button._qv64e._iokts._4tgw8')
    }



    
    if (Math.random() > .9) {


        isItCommentable = yield nightmare.evaluate(function () {
            if (document.querySelector('textarea._bilrf') != null)
                return true;
            else
                return false;
        });
        if (isItCommentable) {
            var comments = [
                ' عالی', ' زیبا', 'پیج زیبایی دارید', 'عجب پیج جالبی', 'موفق باشید', 'آرزوی بهترین ها رو دارم براتون',
                , 'آرزوی بهترین ها ', 'از پیج ما دیدن کنید', '@reservina.ir', 'جالب', 'پیج زیبا', 'لطفا از ماهم حمایت کنید',
                'ازصفحه ما نیز دیدن فرمایید', 'رزرو آرایشگاه', 'رزرو آنلاین آرایشگاه', 'رزرواسیون آرایشگاه', 'عالیه', 'کاملا درسته',
                'دقیقا', 'واقعا', 'درسته', 'ان شا الله', 'رزروینا', 'از بات ما استفاده کنید'
            ];
            // var comments = [
            //     ' عالی', ' زیبا', 'پیج زیبایی دارید', 'عجب پیج جالبی', 'موفق باشید', 'آرزوی بهترین ها رو دارم براتون',
            //     , 'آرزوی بهترین ها ', 'از پیج ما دیدن کنید', 'جالب', 'پیج زیبا', 'لطفا از ماهم حمایت کنید',
            //     'ازصفحه ما نیز دیدن فرمایی','عالیه','کاملا درسته',
            //     'دقیقا','اینجا ایران است','واقعا', 'درسته', 'ان شا الله'
            // ];
            var key = Math.floor((Math.random() * comments.length) + 1);
            yield nightmare
                .type('textarea._bilrf', comments[key])
                .wait()
                .type('textarea._bilrf', '\u000d')

        }

    }
    // var loop = 0;
    // while (loop < 1000) {
    //     yield nightmare.wait();
    //     loop++;
    // }
    return {};

};

function instagram() {
    this.likePosts = function (searchInput) {
        count = 0;
        // create nightmare object with type interval 20 and set chrome as its useragent
        var nightmare = Nightmare({
            typeInterval: 20,
            pollInterval: 0,
            // show: true
        }).useragent('chrome');
        // try to open cookie file inorder to check if it is empty or not
        const stats = fs.statSync("app/public/cookies/instagram/cookie.json")
        const fileSizeInBytes = stats.size;
        if (fileSizeInBytes == 0) {
            // becuase cookie file is empty there is no need to continue
            return false;
        }

        // open cookie file and read it to set it on the agent
        var cookies_raw = fs.readFileSync('app/public/cookies/instagram/cookie.json', 'utf8');
        var cookies = JSON.parse(cookies_raw);

        // return nightmare object to test it's result 
        nightmare.cookies.clearAll()
            .gotoReady('https://www.instagram.com/')
            .cookies.set(cookies);

        return vo(posts(searchInput, nightmare)).then((res) => {
            return res;
        })
            .catch(e => console.error(e))


    }

}

module.exports = instagram;