var Nightmare = require('./BrandNewNightmare');
var fs = require('fs');
var vo = require('vo');

function foursquare() {
    this.searchName = function (searchInput) {

        // create nightmare object with type interval 20 and set chrome as its useragent
        var nightmare = Nightmare({
            typeInterval: 20,
            pollInterval: 0,
            // show: true
        }).useragent('chrome');
        // try to open cookie file inorder to check if it is empty or not
        const stats = fs.statSync("app/public/cookies/foursquare/cookie.json")
        const fileSizeInBytes = stats.size;
        if (fileSizeInBytes == 0) {
            // becuase cookie file is empty there is no need to continue
            return false;
        }

        // open cookie file and read it to set it on the agent
        var cookies_raw = fs.readFileSync('app/public/cookies/foursquare/cookie.json', 'utf8');
        var cookies = JSON.parse(cookies_raw);

        // return nightmare object to test it's result 
        return nightmare.cookies.clearAll()
            .gotoReady('https://www.foursquare.com/')
            .cookies.set(cookies)
            .gotoReady(`https://foursquare.com/search?q=${searchInput}`)
            .wait('.wideColumn')
            .evaluate(() => {
                var result = [];
                document.querySelectorAll('div.searchResult').forEach(function (el) {

                    let subResult = {}
                    if (el.querySelector('div.icon img') != null)
                        subResult['image'] = el.querySelector('div.icon img').src;
                    if (el.querySelector('div.info .name a') != null)
                        subResult['name'] = el.querySelector('div.info .name a').innerHTML;
                    if (el.querySelector('div.info .name a') != null)
                        subResult['link'] = el.querySelector('div.info .name a').href;
                    if (el.querySelector('div.info .address') != null)
                        subResult['address'] = el.querySelector('div.info .address').innerHTML;

                    result.push(subResult);
                });
                return result
            })
            .end()
            .then(res => res)
    }

}

module.exports = foursquare;