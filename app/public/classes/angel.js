var Nightmare = require('./BrandNewNightmare');
var fs = require('fs');
var vo = require('vo');

function angel() {
    this.searchName = function (searchInput) {

        // create nightmare object with type interval 20 and set chrome as its useragent
        var nightmare = Nightmare({
            typeInterval: 20,
            pollInterval: 0,
            // show: true
        }).useragent('chrome');

        // return nightmare object to test it's result 
        return nightmare.cookies.clearAll()
            .goto(`https://angel.co/search?authenticity_token=UpGrDlczQsGQCl6oSKDHs8yIRhnEXx%2FYuPqMJn28QmWVqfwI78ScUPoXTigElUAqMt0sKZNFozQArcJazHTU5Q%3D%3D&page=1&q=${searchInput}&type=people&utf8=%E2%9C%93`)
            .wait('div.section.g-module_section')
            .wait(2000)
            .evaluate(() => {
                var result = [];
                document.querySelectorAll('div.section.g-module_section').forEach(function (el) {
                    let subResult = {}
                    if (el.querySelector('img') != null)
                        subResult['image'] = el.querySelector('img').src;
                    if (el.querySelector('div.title') != null) 
                        subResult['name'] = el.querySelector('div.title').innerText;
                    if (el.querySelector('a') != null)
                        subResult['link'] = el.querySelector('a').href;                    
                    if (el.querySelector('div.type') != null)
                        subResult['description'] = el.querySelector('div.type').innerText;
                    result.push(subResult);
                });
                return result
            })
            .end()
            .then(res => res)
    }

}

module.exports = angel;