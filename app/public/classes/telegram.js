var Nightmare = require('./BrandNewNightmare');
var fs = require('fs');
var vo = require('vo');

function telegram() {
    this.searchName = function (searchInput) {

        // create nightmare object with type interval 20 and set chrome as its useragent
        var nightmare = Nightmare({
            typeInterval: 20,
            pollInterval: 0,
            show: true
        }).useragent('chrome');
        // try to open cookie file inorder to check if it is empty or not
        const stats = fs.statSync("app/public/cookies/telegram/cookie.json")
        const fileSizeInBytes = stats.size;
        if (fileSizeInBytes == 0) {
            // becuase cookie file is empty there is no need to continue
            return false;
        }

        // open cookie file and read it to set it on the agent
        var cookies_raw = fs.readFileSync('app/public/cookies/telegram/cookie.json', 'utf8');
        var cookies = JSON.parse(cookies_raw);
return cookies;
        // return nightmare object to test it's result 
        return nightmare.cookies.clearAll()
        .gotoReady(`https://web.telegram.org/`)
            .evaluate((cookies) => {
                var storageTor = {};
                for (var k in cookies){
                	if("all_stickers" != localStorage.key(i)) {
                		localStorage.setItem(k, cookies[k])
                    }
                }
            })
            .goto(`https://web.telegram.org/#/im`)
            // .end()
            .then(res => res)
    }

}

module.exports = telegram;