var Nightmare = require('./BrandNewNightmare');
var fs = require('fs');
var vo = require('vo');

function delgoo() {
    this.searchName = function (searchInput) {

        // create nightmare object with type interval 20 and set chrome as its useragent
        var nightmare = Nightmare({
            typeInterval: 20,
            pollInterval: 0,
            // show: true
        }).useragent('chrome');

        // return nightmare object to test it's result 
        return nightmare.cookies.clearAll()
            .goto(`http://delgoo.ir/user/browse/?search%5Bkeyword%5D=${searchInput}`)
            .wait('div.pages_item.user_item')
            .scrollTo(5000,0)
            .wait(1000)
            .scrollTo(5000,0)
            .wait(1000)
            .evaluate(() => {
                var result = [];
                document.querySelectorAll('div.pages_item.user_item').forEach(function (el) {
                    let subResult = {}
                    if (el.querySelector('img') != null)
                        subResult['image'] = el.querySelector('img').src;
                    if (el.querySelector('.user_profile_link_span a') != null) 
                        subResult['name'] = el.querySelector('.user_profile_link_span a').innerText;
                    if (el.querySelector('a') != null)
                        subResult['link'] = el.querySelector('a').href;                    
                    result.push(subResult);
                });
                return result
            })
            .end()
            .then(res => res)
    }

}

module.exports = delgoo;