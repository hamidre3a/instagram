var Nightmare = require('./BrandNewNightmare');
var fs = require('fs');
var vo = require('vo');

function vimeo() {
    this.searchName = function (searchInput) {

        // create nightmare object with type interval 20 and set chrome as its useragent
        var nightmare = Nightmare({
            typeInterval: 20,
            pollInterval: 0,
            // show: true
        }).useragent('chrome');

        // return nightmare object to test it's result 
        return nightmare.cookies.clearAll()
            .gotoReady(`https://vimeo.com/search/people?q=${searchInput}`)
            .wait('div.person_thumbnail')
            .evaluate(() => {
                var result = [];
                document.querySelectorAll('div.person_thumbnail').forEach(function (el) {
                    let subResult = {}
                    if (el.querySelectorAll('img.avatar') != null)
                        subResult['image'] = el.querySelector('img.avatar').src;
                    if (el.querySelectorAll('a.user_name') != null) {
                        subResult['name'] = el.querySelector('a.user_name').innerText;
                        subResult['link'] = el.querySelector('a.user_name').href;
                    }
                    if (el.querySelectorAll('span.profile_data.meta') != null)
                        subResult['description'] = el.querySelector('span.profile_data.meta').innerText;
                    result.push(subResult);
                });

                return result;

            })
            .end()
            .then(res => res)
    }

}

module.exports = vimeo;