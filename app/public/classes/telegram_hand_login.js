var Nightmare = require('./BrandNewNightmare');
var fs = require('fs');


function telegram_hand_login() {
    

  this.doLogin = function(){

        // create nightmare object with type interval 20 and set chrome as its useragent
        var nightmare = Nightmare({
        typeInterval: 20,
        show: true
        }).useragent('chrome');
        // open main page of telegarm and insert our login inputs and return it 
        return nightmare.cookies.clearAll().goto('https://web.telegram.org')
        // .wait(4000)
        // .type('input[name="phone_number"]','9903325589')
        // .click('a.login_head_submit_btn')
        .wait(30000)
        .evaluate(() => {
            var storageTor = {};
            for (var i = 0; i < localStorage.length; i++){
                if("all_stickers" != localStorage.key(i)) {
                    storageTor[localStorage.key(i)] = localStorage.getItem(localStorage.key(i))
                }
            }
            return storageTor;
            
        })
        .then((res)=>{
            
            fs.writeFile("app/public/cookies/telegram/cookie.json", JSON.stringify(res), 'utf8', function(error) {
                return true;
            });
            // return output message
            return res
            ;
        })
        .catch(()=>'App could not login');
  }

}

module.exports = telegram_hand_login;