var Nightmare = require('./BrandNewNightmare');
var fs = require('fs');
var vo = require('vo');

function nlai() {
    this.searchName = function (searchInput) {

        // create nightmare object with type interval 20 and set chrome as its useragent
        var nightmare = Nightmare({
            typeInterval: 20,
            pollInterval: 0,
            // show: true
        }).useragent('chrome');

        // return nightmare object to test it's result 
        return nightmare.cookies.clearAll()
            .gotoReady(`http://opac.nlai.ir/opac-prod/search/bibliographicSimpleSearchProcess.do?simpleSearch.value=${searchInput}&bibliographicLimitQueryBuilder.biblioDocType=&simpleSearch.indexFieldId=&nliHolding=&command=I&simpleSearch.tokenized=true&classType=0&pageStatus=0&bibliographicLimitQueryBuilder.useDateRange=null&bibliographicLimitQueryBuilder.year=&documentType=&attributes.locale=fa`)
            .wait('#table tr')
            .evaluate(() => {

                var result = [];
                document.querySelectorAll('#table tr').forEach(function (el) {
                    let subResult = {}
                    if (el.querySelectorAll('td.gridrownormal3')[1] != null)
                        subResult['type'] = el.querySelectorAll('td.gridrownormal3')[1].innerText;
                    if (el.querySelector('td#td2.gridrownormal3 a') != null)
                        subResult['title'] = el.querySelector('td#td2.gridrownormal3 a').innerHTML;
                    if (el.querySelector('td#td2.gridrownormal3 a') != null)
                        subResult['link'] = el.querySelector('td#td2.gridrownormal3 a').href;
                    if (el.querySelector('td#td3.gridrownormal3') != null)
                        subResult['name'] = el.querySelector('td#td3.gridrownormal3').innerHTML;
                    if (el.querySelector('td#td4.gridrownormal3 input') != null)
                        subResult['time'] = el.querySelector('td#td4.gridrownormal3 input').value;
                    result.push(subResult);
                });

                return result
            })
            .end()
            .then(res => res)
    }

}

module.exports = nlai;