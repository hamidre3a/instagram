var Nightmare = require('./BrandNewNightmare');
var fs = require('fs');
var vo = require('vo');

function profile() {
    this.searchName = function (searchInput) {

        // create nightmare object with type interval 20 and set chrome as its useragent
        var nightmare = Nightmare({
            typeInterval: 20,
            pollInterval: 0,
            // show: true
        }).useragent('chrome');

        // return nightmare object to test it's result 
        return nightmare.cookies.clearAll()
            .goto(`https://profile.ir/search?t=pr&q=${searchInput}`)
            .wait('li.pd-profile-box')
            .wait(4000)
            .evaluate(() => {
                var result = [];
                document.querySelectorAll('li.pd-profile-box').forEach(function (el) {
                    let subResult = {}
                    if (el.querySelector('img.ng-isolate-scope') != null)
                        subResult['image'] = el.querySelector('img.ng-isolate-scope').src;
                    if (el.querySelector('a.pbox-name') != null) {
                        subResult['name'] = el.querySelector('a.pbox-name').innerText;
                        subResult['link'] = el.querySelector('a.pbox-name').href;
                    }
                    if (el.querySelector('span.pbox-desc') != null)
                        subResult['description'] = el.querySelector('span.pbox-desc').innerText;
                    result.push(subResult);
                });
                return result
            })
            .end()
            .then(res => res)
    }

}

module.exports = profile;