var express = require('express');
var router = express.Router();
var facebook = require('../public/classes/facebook');
var profile = require('../public/classes/profile');
var delgoo = require('../public/classes/delgoo');
var angel = require('../public/classes/angel');
var vimeo = require('../public/classes/vimeo');
var facebook_login = require('../public/classes/facebook_login');
var atbox_login = require('../public/classes/atbox_login');
var atbox = require('../public/classes/atbox');
var telegram = require('../public/classes/telegram');
// 09903325589
router.get('/telegram/:name',function(req , routeRes){
  var telegram_obj = new telegram();
  
  var result = telegram_obj.searchName(req.params.name);
  console.log(result);routeRes.status(200).json(result);
  // result.then((final_array) => {
  //   return routeRes.status(200).json(final_array);
  // })
  // .catch((res) => {
  //   console.log('why am i here', res);
  //    return routeRes.status(200).send();
  // });
})

router.get('/api/vimeo/search/:name', function(req, routeRes){
  var vimeo_obj = new vimeo();
  var result = vimeo_obj.searchName(req.params.name);
  result.then((final_array) => {
    return routeRes.status(200).json(final_array);
  })
  .catch((res) => {
    console.log('why am i here', res);
     return routeRes.status(200).send();
  });
});

router.get('/api/atb/search/:name', function (req, routeRes) {

  // var login_obj = new atbox_login();
  // var result = login_obj.checkLogin();
  // if (typeof result === 'object') {
  //   console.log('atbox_login result is object');
  //   //The result is some kind of object so we assume it's a promise
  //   result.then((res) => {
  //     console.log('and it is successful');
      // if (res === true) {
        // App is logged in || success case 
        var atbox_obj = new atbox();
        var result = atbox_obj.searchName(req.params.name);
        result.then((final_array) => {
          return routeRes.status(200).json(final_array);
        })
        .catch((res) => { 
          console.log('why am i here', res); 
           return routeRes.status(200).send();
          // return routeRes.status(200).json([{ 'status': 404 }]);
         });


  //     } else {
  //       console.log('but it is not successful');
  //       // App is not logged in because we couldn't find our target element on the page
  //       let loginResult = login_obj.doLogin()
  //       loginResult
  //         .then(console.log)
  //         .catch(console.log);
  //     }
  //   })
  //     .catch(e => console.error(e))
  // } else if (result === false) {
  //   // App is not logged in because cookie file is empty
  //   let loginResult = login_obj.doLogin()
  //   loginResult
  //     .then(console.log)
  //     .catch(console.log);
  // }

});


router.get('/api/ang/search/:name', function (req, routeRes) {
  var angel_obj = new angel();
  var result = angel_obj.searchName(req.params.name);
  result.then((final_array) => {

    return routeRes.status(200).json(final_array);
  })
  .catch((res) => {
    console.log('why am i here', res);
     return routeRes.status(200).send();
  });
});



router.get('/api/del/search/:name', function (req, routeRes) {
  var delgoo_obj = new delgoo();
  var result = delgoo_obj.searchName(req.params.name);
  result.then((final_array) => {

    return routeRes.status(200).json(final_array);
  })
  .catch((res) => {
    console.log('why am i here', res);
     return routeRes.status(200).send();
  });
});


router.get('/api/pro/search/:name', function (req, routeRes) {
  var profile_obj = new profile();
  var result = profile_obj.searchName(req.params.name);
  result.then((final_array) => {

    return routeRes.status(200).json(final_array);
  })
  .catch((res) => {
    console.log('why am i here', res);
     return routeRes.status(200).send();
  });
});


router.get('/api/fac/search/:name', function (req, routeRes) {

  // var login_obj = new facebook_login();
  // var result = login_obj.checkLogin();
  // if (typeof result === 'object') {
  //   //The result is some kind of object so we assume it's a promise
  //   result.then((res) => {
  //     console.log('and it is successful');
  //     if (res === true) {
        // App is logged in || success case 
        var facebook_obj = new facebook();
        var result = facebook_obj.searchName(req.params.name);
        result.then((final_array) => {
          return routeRes.status(200).json(final_array);
        }).catch((res) => { 
          console.log('why am i here', res); 
          return routeRes.status(200).send(); 
        });


  //     } else {
  //       console.log('but it is not successful');
  //       // App is not logged in because we couldn't find our target element on the page
  //       let loginResult = login_obj.doLogin()
  //       loginResult
  //         .then(console.log)
  //         .catch(console.log);
  //     }
  //   })
  //     .catch(e => console.error(e))
  // } else if (result === false) {
  //   // App is not logged in because cookie file is empty
  //   let loginResult = login_obj.doLogin()
  //   loginResult
  //     .then(console.log)
  //     .catch(console.log);
  // }

});

module.exports = router;
