var Service = require('node-windows').Service;

// Create a new service object
var svc = new Service({
  name:'instagram',
  description: 'The Instagram server.',
  script: 'E:\\instagram\\start.js'
});

// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install',function(){
  svc.start();
});

svc.install();